// ignore_for_file: file_names, use_build_context_synchronously

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_todoapp/loginForm.dart';
import "package:sqflite/sqflite.dart";

import 'main.dart';

class Signup extends StatefulWidget {
  const Signup({super.key});
  @override
  State<Signup> createState() => SignupState();
}

class SignUp {
  String userName;
  String passWord;
  String email;

  SignUp({
    required this.userName,
    required this.passWord,
    required this.email,
  });

  Map<String, dynamic> signupMap() {
    return {
      'userName': userName,
      'password': passWord,
      'email': email,
    };
  }

  @override
  String toString() {
    return '{username:$userName,password:$passWord},email:$email}';
  }
}

String? userName;
String? passWord;
String? email;

Future addSignUpDetails() async {
  List signUpData = SignupState.data;
  log(signUpData.toString());
  for (int i = 0; i < signUpData.length; i++) {
    userName = signUpData[i]["username"];
    passWord = signUpData[i]["password"];
    email = signUpData[i]["email"];

    await insertSignupData(
        SignUp(userName: userName!, passWord: passWord!, email: email!));
  }

  // print(await getSignupData());
}

Future<void> insertSignupData(SignUp obj) async {
  final localDB = await database;
  await localDB.insert(
    "SignupInfo",
    obj.signupMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
  var data = await getSignupData();
  log("IN INSERT ${data.toString()}");
}

Future<List> getSignupData() async {
  final localDB = await database;

  List<Map<String, dynamic>> signUplist = await localDB.query("SignupInfo");
  log(signUplist.toString());
  return signUplist;
}

class SignupState extends State<Signup> {
  ///CONTROLLERS
  static final TextEditingController userNameController =
      TextEditingController();
  static final TextEditingController passwordController =
      TextEditingController();
  static final TextEditingController emailController = TextEditingController();

  bool isClickEye = true;
  bool isClickEye1 = true;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  static List data = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
      body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          alignment: Alignment.center,
          height: 570,
          width: 370,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                offset: Offset(10, 18),
                blurRadius: 5,
                spreadRadius: 1,
              ),
            ],
          ),
          child: Padding(
            padding: MediaQuery.paddingOf(context),
            // padding: MediaQuery.viewInsetsOf(context),
            child: Column(
              children: [
                Text(
                  " Sign Up ",
                  style: GoogleFonts.quicksand(
                      textStyle: const TextStyle(
                    fontSize: 30,
                    color: Color.fromRGBO(89, 57, 241, 1),
                    fontWeight: FontWeight.bold,
                  )),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: 5,
                      ),
                      Image.network(
                        "https://cdn-icons-png.flaticon.com/256/5969/5969099.png",
                        height: 80,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        controller: userNameController,
                        // key: userNameKey,
                        decoration: InputDecoration(
                          hintText: "Enter username",
                          label: const Text("Enter username"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.person,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please Enter username";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.emailAddress,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        controller: emailController,
                        // key: passwordKey,
                        decoration: InputDecoration(
                          hintText: "Enter Email",
                          label: const Text("Enter Email"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.email_rounded,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Enter valid email";
                          } else {
                            return null;
                          }
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        controller: passwordController,
                        // key: passwordKey,
                        obscureText: isClickEye,
                        decoration: InputDecoration(
                          hintText: "Enter password",
                          label: const Text("Enter password"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.lock,
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                isClickEye = !isClickEye;
                              });
                            },
                            icon: isClickEye
                                ? const Icon(
                                    Icons.visibility,
                                    color: Color.fromRGBO(89, 57, 241, 1),
                                  )
                                : const Icon(Icons.visibility_off),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please Enter password";
                          } else {
                            return null;
                          }
                        },
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                          onPressed: () async {
                            bool loginValidated =
                                _formKey.currentState!.validate();

                            if (loginValidated) {
                              data.add({
                                "username": userNameController.text,
                                "password": passwordController.text,
                                "email": emailController.text,
                              });
                              await addSignUpDetails();
                              userNameController.clear();
                              passwordController.clear();
                              emailController.clear();

                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  duration: Duration(milliseconds: 400),
                                  backgroundColor: Colors.green,
                                  content: Text("Sign Up Successful"),
                                ),
                              );
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => const LoginPage()));
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  duration: Duration(milliseconds: 400),
                                  backgroundColor: Colors.red,
                                  content: Text("SignUp Failed"),
                                ),
                              );
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromRGBO(89, 57, 241, 1),
                            foregroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: const Text(
                            " Sign Up ",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Already have Account ?",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => const LoginPage()));
                            },
                            child: const Text(
                              "Login",
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
