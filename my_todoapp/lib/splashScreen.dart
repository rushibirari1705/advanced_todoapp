
import "dart:async";

import "package:flutter/material.dart";
import "loginForm.dart";

class SplashScreen extends StatefulWidget{

  const SplashScreen({super.key});
  @override
  State createState(){ return _SplashScreenState();}

}


class _SplashScreenState extends State{

  @override
  void initState(){
    super.initState();
    Timer(const Duration(milliseconds: 400), (){
      Navigator.pushReplacement(context, MaterialPageRoute(builder:(context)=> const LoginPage()));
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
        Image.asset("assets/v3.png",height: 700, width: 800,),
      ],)),
    );
  }
}