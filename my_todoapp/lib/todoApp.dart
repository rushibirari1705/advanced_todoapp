
// ignore_for_file: file_names
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'loginForm.dart';
import 'main.dart';

class MyToDoApp extends StatefulWidget {
  const MyToDoApp({super.key});

  @override
  State<MyToDoApp> createState() => _MyToDoApp();
}

class _MyToDoApp extends State<MyToDoApp> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController datePickerController = TextEditingController();
  TextEditingController timePickerController = TextEditingController();

  bool titleValidate = false;
  bool desValidate = false;
  bool dateValidate = false;
  bool timeValidate = false;

  // bool _onclick = true;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List<ToDoModelClass> cardlist = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(milliseconds: 50),
      () async {
        cardlist = await getCards();
        setState(() {});
      },
    );
  }

  Future insertCardData(ToDoModelClass obj) async {
    final localDB = await taskdatabase;
    await localDB.insert(
      "cardsDetails",
      obj.modelMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteCardFromDb(ToDoModelClass obj) async {
    final localDB = await taskdatabase;
    await localDB
        .delete('cardsDetails', where: "title = ?", whereArgs: [obj.title]);

    cardlist = await getCards();
  }

  Future<void> updateCardFromDb(ToDoModelClass obj) async {
    final localDB = await taskdatabase;
    await localDB.update('cardsDetails', obj.modelMap(),
        where: "title = ?", whereArgs: [obj.title]);

    cardlist = await getCards();
  }

  void addCards() async {
    setState(() {});
    cardlist = await getCards();
  }

  void isSubmitData() async {
    String enteredTitle = titleController.text.trim();
    String enteredDescription = descriptionController.text.trim();
    String enteredDate = datePickerController.text.trim();
    String enteredTime = timePickerController.text.trim();

    enteredTitle.isEmpty ? titleValidate = true : titleValidate = false;
    enteredDescription.isEmpty ? desValidate = true : desValidate = false;
    enteredDate.isEmpty ? dateValidate = true : dateValidate = false;
    enteredTime.isEmpty ? timeValidate = true : timeValidate = false;

    if (dateValidate == false &&
        titleValidate == false &&
        desValidate == false &&
        timeValidate == false) {
      setState(() {
        insertCardData(ToDoModelClass(
          title: titleController.text,
          description: descriptionController.text,
          date: datePickerController.text,
          time: timePickerController.text,
          isComplete: false,
        ));
      });

      Navigator.pop(context);
      setState(() async {
        cardlist = await getCards();
        clearControllers();
        addCards();
      });

      titleValidate = false;
      desValidate = false;
      dateValidate = false;
      timeValidate = false;

      clearControllers();

      Navigator.pop(context);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          
          backgroundColor: Color.fromRGBO(2, 147, 33, 0.741),
          content: Text("Task added Successfully"),
        ),
      );
    } else {
      Navigator.pop(context);
      clearControllers();
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Color.fromRGBO(255, 51, 0, 0.878),
          content: Text("Task Failed"),
        ),
      );
    }
  }

  void clearControllers() {
    titleController.clear();
    descriptionController.clear();
    datePickerController.clear();
    timePickerController.clear();
  }

  // ignore: non_constant_identifier_names
  List color_list = const [
    Color.fromRGBO(255, 249, 244, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1),
  ];

  Future addBottomsheet() {
    setState(() {});
    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Form(
              key: _formKey,
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Center(
                      child: Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(" Create Task ",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 22,
                              color: Color.fromRGBO(89, 57, 241, 1),
                            )),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      height: 60,
                      width: double.infinity,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 5),
                      child: TextFormField(
                        controller: titleController,
                        // key: userNameKey,
                        decoration: InputDecoration(
                          hintText: " Enter Title",
                          label: const Text(" Title"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.note_add_outlined,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter title";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.text,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 100,
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                      child: TextFormField(
                        controller: descriptionController,
                        // key: passwordKey,
                        decoration: InputDecoration(
                          hintText: "Enter Description",
                          label: const Text("Enter Description"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.notes_sharp,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter Description";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 380,
                      margin: const EdgeInsets.fromLTRB(20, 2, 15, 0),
                      child: TextFormField(
                        controller: datePickerController,
                        decoration: InputDecoration(
                          errorText:
                              dateValidate ? 'Value Cannot Be Empty' : null,
                          prefixIcon: const Icon(Icons.calendar_month_rounded),
                          hintText: "Select Date",
                          //border: Border.all(width: 5),
                        ),
                        readOnly: true,
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2023),
                              lastDate: DateTime(2100));

                          String formatedDate =
                              DateFormat.yMMMd().format(pickedDate!);

                          setState(() {
                            datePickerController.text = formatedDate;
                          });
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 50,
                      width: 380,
                      margin: const EdgeInsets.fromLTRB(20, 0, 15, 0),
                      child: TextFormField(
                        controller: timePickerController,
                        decoration: InputDecoration(
                            errorText:
                                timeValidate ? 'Value Cannot Be Empty' : null,
                            prefixIcon: const Icon(Icons.timer),
                            hintText: "Select Time"),
                        readOnly: true,
                        onTap: () async {
                          TimeOfDay? pickedTime = await showTimePicker(
                            context: context,
                            initialTime: const TimeOfDay(hour: 5, minute: 30),
                          );

                          String formattedTime =
                              '${pickedTime?.hour.toString().padLeft(2, '0')}:${pickedTime?.minute.toString().padLeft(2, '0')}';

                          setState(() {
                            timePickerController.text = formattedTime;
                          });
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: SizedBox(
                        height: 50,
                        width: 300,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  isSubmitData();
                                  addCards();
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor:
                                    const Color.fromRGBO(89, 57, 241, 1),
                                foregroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              child: const Text(
                                " Add ",
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }


  Future editBottomsheet(
    ToDoModelClass obj,
    int index,
  ) {
    titleController.text = obj.title;
    descriptionController.text = obj.description;
    datePickerController.text = obj.date;
    timePickerController.text = obj.time;
    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Form(
              key: _formKey,
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Center(
                      child: Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text("  Update Task ",
                            style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 22,
                              color: Color.fromRGBO(89, 57, 241, 1),
                            )),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      height: 60,
                      width: double.infinity,
                      alignment: Alignment.center,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 5),
                      child: TextFormField(
                        controller: titleController,
                        // key: userNameKey,
                        decoration: InputDecoration(
                          hintText: " Enter Title",
                          label: const Text(" Title"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.note_add_outlined,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter title";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.text,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 100,
                      width: double.infinity,
                      margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                      child: TextFormField(
                        controller: descriptionController,
                        // key: passwordKey,
                        decoration: InputDecoration(
                          hintText: "Enter Description",
                          label: const Text("Enter Description"),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          prefixIcon: const Icon(
                            Icons.notes_sharp,
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Please enter Description";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 380,
                      margin: const EdgeInsets.fromLTRB(20, 2, 15, 0),
                      child: TextFormField(
                        controller: datePickerController,
                        decoration: InputDecoration(
                          errorText:
                              dateValidate ? 'Value Cannot Be Empty' : null,
                          prefixIcon: const Icon(Icons.calendar_month_rounded),
                          hintText: "Select Date",
                          //border: Border.all(width: 5),
                        ),
                        readOnly: true,
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2023),
                              lastDate: DateTime(2100));

                          String formatedDate =
                              DateFormat.yMMMd().format(pickedDate!);

                          setState(() {
                            datePickerController.text = formatedDate;
                          });
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: 50,
                      width: 380,
                      margin: const EdgeInsets.fromLTRB(20, 0, 15, 0),
                      child: TextFormField(
                        controller: timePickerController,
                        decoration: InputDecoration(
                            errorText:
                                timeValidate ? 'Value Cannot Be Empty' : null,
                            prefixIcon: const Icon(Icons.timer),
                            hintText: "Select Time"),
                        readOnly: true,
                        onTap: () async {
                          TimeOfDay? pickedTime = await showTimePicker(
                            context: context,
                            initialTime: const TimeOfDay(hour: 5, minute: 30),
                          );

                          String formattedTime =
                              '${pickedTime?.hour.toString().padLeft(2, '0')}:${pickedTime?.minute.toString().padLeft(2, '0')}';

                          setState(() {
                            timePickerController.text = formattedTime;
                          });
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: SizedBox(
                        height: 50,
                        width: 300,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  String enteredTitle =
                                      titleController.text.trim();
                                  String enteredDescription =
                                      descriptionController.text.trim();
                                  String enteredDate =
                                      datePickerController.text.trim();
                                  String enteredTime =
                                      timePickerController.text.trim();

                                  enteredTitle.isEmpty
                                      ? titleValidate = true
                                      : titleValidate = false;
                                  enteredDescription.isEmpty
                                      ? desValidate = true
                                      : desValidate = false;
                                  enteredDate.isEmpty
                                      ? dateValidate = true
                                      : dateValidate = false;
                                  enteredTime.isEmpty
                                      ? timeValidate = true
                                      : timeValidate = false;

                                  if (dateValidate == false &&
                                      titleValidate == false &&
                                      desValidate == false &&
                                      timeValidate == false) {
                                    // getCards();
                                    setState(() {
                                      cardlist[index] = ToDoModelClass(
                                        title: titleController.text,
                                        description: descriptionController.text,
                                        date: datePickerController.text,
                                        time: timePickerController.text,
                                        isComplete: false,
                                      );
                                    });

                                    clearControllers();

                                    Navigator.pop(context);
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        backgroundColor:
                                            Color.fromRGBO(91, 2, 147, 0.745),
                                        content:
                                            Text("Task Updated Successfully"),
                                      ),
                                    );
                                  } else {
                                    Navigator.pop(context);
                                    setState(() {});
                                    clearControllers();

                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content: Text("Task Not Updated"),
                                      ),
                                    );
                                  }
                                });
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor:
                                    const Color.fromRGBO(89, 57, 241, 1),
                                foregroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              child: const Text(
                                " Update ",
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget addListViewBuilder() {
    setState(() {});
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: cardlist.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
          child: Container(
            width: double.infinity,
            height: 110,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: Color.fromARGB(255, 247, 247, 247),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  offset: Offset(0, 8),
                  blurRadius: 5,
                  spreadRadius: 1,
                ),
              ],
            ),
            child: Slidable(
              endActionPane: ActionPane(
                extentRatio: 0.2,
                motion: const ScrollMotion(),
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 28.0),
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () {
                            setState(() {
                              editBottomsheet(cardlist[index], index);
                            });
                          },
                          icon: const Icon(Icons.edit),
                          color: const Color.fromRGBO(89, 57, 241, 1),
                        ),
                        const SizedBox(
                          height: 2,
                        ),
                        IconButton(
                          onPressed: () {
                            showAlertDialog(BuildContext context) {
                              Widget cancelButton = TextButton(
                                child: const Text("Cancel"),
                                onPressed: () {
                                  setState(() {
                                    Navigator.of(context).pop();
                                  });
                                },
                              );

                              Widget deleteButton = TextButton(
                                child: const Text("Delete"),
                                onPressed: () {
                                  setState(() {
                                    ToDoModelClass obj = ToDoModelClass(
                                        title: cardlist[index].title,
                                        description:
                                            cardlist[index].description,
                                        date: cardlist[index].date,
                                        time: cardlist[index].time,
                                        isComplete: cardlist[index].isComplete);
                                    deleteCardFromDb(obj);
                                    cardlist.removeAt(index);

                                    Navigator.of(context).pop();
                                  });
                                  setState(() {});
                                },
                              );

                              AlertDialog alert = AlertDialog(
                                title: const Text("Alert Dialog"),
                                content: const Text("Are you sure to delete ?"),
                                actions: [
                                  cancelButton,
                                  deleteButton,
                                ],
                              );
                              // show the dialog
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return alert;
                                },
                              );
                            }

                            setState(() {
                              showAlertDialog(context);
                            });
                          },
                          icon: const Icon(Icons.delete),
                          color: const Color.fromRGBO(89, 57, 241, 1),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              child: Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Container(
                          height: 52,
                          width: 52,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            border: Border.all(width: 0.3),
                            shape: BoxShape.circle,
                            color: color_list[index % color_list.length],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 0.0,
                            ),
                            child: Text(
                              "${index + 1}",
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(55, 58, 91, 1),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(width: 10),
                  SizedBox(
                    width: 250,
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                                child: Text(
                              cardlist[index].title,
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                            )),
                          ],
                        ),
                      ),
                      const SizedBox(height: 2),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                              cardlist[index].description,
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                              )),
                            )),
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 20.0),
                        child: Spacer(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Row(
                          children: [
                            Text(
                              cardlist[index].date,
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                            ),
                            const SizedBox(
                              width: 13,
                            ),
                            Text(
                              cardlist[index].time,
                              style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                            ),
                          ],
                        ),
                      ),
                    ]),
                  ),
                  const Spacer(),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: IconButton(
                              iconSize: 30,
                              onPressed: () {
                                setState(() {
                                  cardlist[index].isComplete = true;
                                });
                              },
                              icon: cardlist[index].isComplete
                                  ? const Icon(
                                      Icons.check_circle,
                                      color: Colors.green,
                                    )
                                  : const Icon(
                                      Icons.check_circle_outline,
                                    )),
                        ),
                      ]),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() {});
    return Scaffold(
      backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 50),
          SizedBox(
            height: 30,
            width: 250,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Text(
                "Good Morning , ",
                style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w400,
                  color: Colors.white,
                )),
              ),
            ),
          ),
          SizedBox(
            height: 35,
            width: 200,
            child: Padding(
              padding: const EdgeInsets.only(left: 22.0),
              child: Text(
                LoginPageState.userName,
                style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                )),
              ),
            ),
          ),
          const SizedBox(height: 20),
          Expanded(
            child: Container(
              height: 600,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(217, 217, 217, 1),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                  topLeft: Radius.circular(40),
                ),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "CREATE TO DO LIST ",
                      style: GoogleFonts.quicksand(
                          textStyle: const TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Expanded(
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(40),
                          topLeft: Radius.circular(40),
                        ),
                      ),
                      child: addListViewBuilder(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addBottomsheet,
        tooltip: "Add task",
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        child: const Icon(
          Icons.add,
          size: 40,
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
      ),
    );
  }
}

class ToDoModelClass {
  final String title;
  final String description;
  final String date;
  final String time;
  bool isComplete;

  ToDoModelClass({
    required this.title,
    required this.description,
    required this.date,
    required this.time,
    required this.isComplete,
  });

  Map<String, dynamic> modelMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
      'time': time,
      'isCompleted': isComplete,
    };
  }
}

Future<List<ToDoModelClass>> getCards() async {
  log("IN Get Cards");
  final localDB = await taskdatabase;
  List<Map<String, dynamic>> listCards = await localDB.query("cardsDetails");
  return List.generate(listCards.length, (i) {
    return ToDoModelClass(
        title: listCards[i]['title'],
        description: listCards[i]['description'],
        date: listCards[i]['date'],
        time: listCards[i]['time'],
        isComplete: isDone(listCards[i]['isCompleted']));
  });
}

bool isDone(int str) {
  if (str == '1') {
    if (str == "true") {
      return true;
    }
  }
  return false;
}
