import 'package:flutter/material.dart';
import 'splashScreen.dart';
import "package:sqflite/sqflite.dart";
import "package:path/path.dart";

dynamic database;
dynamic taskdatabase;

void main()async {
  runApp(const MyApp());

  //create database
  
  database = openDatabase(
    join(await getDatabasesPath(), "SignupDB1.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute('''
            CREATE TABLE SignupInfo(
                userName TEXT primary key,
                password TEXT,    
                email TEXT
            )
            ''');
    },
  );

  taskdatabase = openDatabase(join(await getDatabasesPath(), "TaskDatabase1.db"),
      version: 1, onCreate: (db, version) async {
    await db.execute('''
        CREATE TABLE cardsDetails(
          title TEXT PRIMARY KEY,
          description TEXT,
          date TEXT,
          time TEXT,
          isCompleted BOOLEAN
        )
      ''');
  });

}

class MyApp extends StatelessWidget {

  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }


}