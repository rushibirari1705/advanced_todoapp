// ignore_for_file: use_build_context_synchronously

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:my_todoapp/todoApp.dart';
import 'signupForm.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  ///CONTROLLERS
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  static String userName = "";

  late Size mSize;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool isClickEye = true;

  List<String> values = [];

  Future<bool> checkUserData() async {
    List signUplist = await getSignupData();

    for (int i = 0; i < signUplist.length; i++) {
      List retval = getKeysAndValuesUsingEntries(signUplist[i]);
      log("here");
      for (int i = 0; i < retval.length; i++) {
        if (values[i] == usernameController.text &&
            values[i + 1] == passwordController.text) {
          userName = values[i];
          return true;
        }
      }
    }
    return false;
  }

  List getKeysAndValuesUsingEntries(Map map) {
    for (var value in map.values) {
      values.add(value);
    }
    log(values.toString());
    return values;
  }

  @override
  Widget build(BuildContext context) {
    mSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
      body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          alignment: Alignment.center,
          height: 460,
          width: 370,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                offset: Offset(10, 18),
                blurRadius: 5,
                spreadRadius: 1,
              ),
            ],
          ),
          child: Column(
            children: [
              Text(
                " Login ",
                style: GoogleFonts.quicksand(
                    textStyle: const TextStyle(
                  fontSize: 30,
                  color: Color.fromRGBO(89, 57, 241, 1),
                  fontWeight: FontWeight.bold,
                )),
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                    Image.network(
                      //"https://w7.pngwing.com/pngs/340/946/png-transparent-avatar-user-computer-icons-software-developer-avatar-child-face-heroes-thumbnail.png",
                      "https://cdn-icons-png.flaticon.com/512/295/295128.png",
                      width: 80,
                      height: 80,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: usernameController,
                      // key: userNameKey,
                      decoration: InputDecoration(
                        hintText: "Enter username",
                        label: const Text("Enter username"),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        prefixIcon: const Icon(
                          Icons.person,
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter username";
                        } else {
                          return null;
                        }
                      },
                      keyboardType: TextInputType.emailAddress,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: passwordController,
                      // key: passwordKey,
                      obscureText: isClickEye,
                      obscuringCharacter: "*",
                      decoration: InputDecoration(
                        hintText: "Enter password",
                        label: const Text("Enter password"),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        prefixIcon: const Icon(
                          Icons.lock,
                        ),
                        suffixIcon: IconButton(
                          onPressed: () {
                            setState(() {
                              isClickEye = !isClickEye;
                            });
                          },
                          icon: isClickEye
                              ? const Icon(
                                  Icons.visibility,
                                  color: Color.fromRGBO(89, 57, 241, 1),
                                )
                              : const Icon(Icons.visibility_off),
                        ),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter password";
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                        onPressed: () async {
                          bool loginValidated =
                              _formKey.currentState!.validate();
                          log(loginValidated.toString());
                          if (loginValidated) {
                            if (await checkUserData()) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  duration: Duration(milliseconds: 400),
                                  content: Text("Login Successful"),
                                  backgroundColor: Colors.green,
                                ),
                              );

                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => const MyToDoApp()));
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  duration: Duration(milliseconds: 400),
                                  content: Text("Login Failed"),
                                  backgroundColor: Colors.red,
                                ),
                              );
                            }
                          }
                          setState(() {});
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
                          foregroundColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text(
                          " Login ",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        )),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "Create Account ? ",
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const Signup()));
                          },
                          child: const Text(
                            "Sign Up",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
